

//Validar formulario login

$(function () {
  $("#formulario-login").validate({
    rules: {
      inputEmail: {
        required: true,
      },
      contrasenia: {
        required: true
      }
    },
    messages: {
      inputEmail: {
        required: "Debe ingresar su correo",
        email: "Debe ingresar un correo con formato abc@abc.cl"
      },
      inputPassword: {
        required: "Ingrese contraseña",
      }
    }
  })
})


(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();