from django.urls import path,include
from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url
from django.contrib import admin
from django.views.generic import TemplateView
from django.contrib.auth import views as auth_views



urlpatterns = [
    path('', views.index, name="index"),


    path('registroCliente/', views.registroCliente, name="registroCliente"),

    path('index/', views.index, name="index"),
    path('index/login_iniciar',views.login_iniciar,name="login_iniciar"),
    path('cerrarsession',views.cerrar_session,name="cerrar_session"),
    
    path('registroCliente/', views.registroCliente, name="registroCliente"),
    path('registroCliente/crear_cliente/', views.crear_cliente, name="cliente"),
    path('listarCliente/', views.listarCliente, name="listarCliente"),
    path('listarCliente/eliminar_cliente/<int:id>', views.eliminar_cliente, name="eliminar_cliente"),
    path('listarCliente/modificar_cliente/<int:id>', views.modificar_cliente, name="modificar_cliente"),
    path('listarCliente/administrarCliente/<int:id>', views.administrarCliente, name="administrarCliente"),


    path('reset_password/',
     auth_views.PasswordResetView.as_view(template_name="password_reset.html"),
     name="reset_password"),

    path('reset_password_sent/', 
        auth_views.PasswordResetDoneView.as_view(template_name="password_reset_sent.html"), 
        name="password_reset_done"),

    path('reset/<uidb64>/<token>/',
     auth_views.PasswordResetConfirmView.as_view(template_name="password_reset_form.html"), 
     name="password_reset_confirm"),

    path('reset_password_complete/', 
        auth_views.PasswordResetCompleteView.as_view(template_name="password_reset_done.html"), 
        name="password_reset_complete"),

]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
