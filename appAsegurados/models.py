from django.db import models

# Create your models here.


# Clase registro cliente 
class RegistroCliente(models.Model):

    nombreEmpresa = models.CharField(max_length=50)
    rubro = models.CharField(max_length=50) 
    rut = models.CharField(max_length=20)
    region = models.CharField(max_length=50)
    ciudad = models.CharField(max_length=50)
    comuna = models.CharField(max_length=50)
    direccion = models.CharField(max_length=50)
    telefono = models.CharField(max_length=50)
    correo = models.CharField(max_length=50)
    contrasenia = models.CharField(max_length=20)
    tipoContrato = models.CharField(max_length=20)
    fechaIngreso = models.CharField(max_length=20)
    
    
    
    