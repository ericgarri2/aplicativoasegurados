# Importar Redireccionamientos
from django.shortcuts import redirect, render
from django.http import HttpResponse
# Importar Redireccionamientos
from django.http import HttpResponse
from django.shortcuts import redirect, render

from .models import RegistroCliente
from django.shortcuts import redirect
# Importar usuario
from django.contrib.auth.models import User
# Sistema de autenticación
from django.contrib.auth import authenticate, logout, login as auth_login
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import AbstractUser, Group
# Create your views here.

# index
def index(request):
    return render(request, 'index.html')


def reset_password (request):
    return render(request, 'reset_password.html')
# Inicio sesion


def registroCliente(request):
    usuario = request.session.get('usuario', None)
    return render(request, 'registroCliente.html', {'name': 'Registro de personas', 'usuario': usuario, })


def login_iniciar(request):
    correo = request.POST.get('correo', '')
    contrasenia = request.POST.get('contrasenia', '')
    user = authenticate(username=correo, password=contrasenia)
    if user is not None:
        auth_login(request, user)
        request.session['usuario'] = user.first_name
        return redirect('registroCliente')
    else:
        return render(request, 'registroCliente.html', {'error': 'Usuario y/o contraseña incorrectos'})


# cerrar_session
@login_required(login_url='login')
def cerrar_session(request):
    del request.session['usuario']
    logout(request)
    return redirect('index')


def crear_cliente(request):
    nombreEmpresa = request.POST.get('nombreEmpresa', '')
    rubro = request.POST.get('rubro', '')
    rut = request.POST.get('rut', '')
    region = request.POST.get('region', '')
    ciudad = request.POST.get('ciudad', '')
    comuna = request.POST.get('comuna', '')
    direccion = request.POST.get('direccion', '')
    telefono = request.POST.get('telefono', '')
    correo = request.POST.get('correo', '')
    contrasenia = request.POST.get('contrasenia', '')
    tipoContrato = request.POST.get('tipoContrato', '')
    fechaIngreso = request.POST.get('fechaIngreso', '')
    registroCliente = RegistroCliente(nombreEmpresa=nombreEmpresa, rubro=rubro, rut=rut, region=region, ciudad=ciudad, comuna=comuna,
                                      direccion=direccion, telefono=telefono, correo=correo, contrasenia=contrasenia, tipoContrato=tipoContrato, fechaIngreso=fechaIngreso)
    registroCliente.save()
    user = User.objects.create_user(username=correo, password=contrasenia,
                                    email=correo, first_name=nombreEmpresa)
    user.save()
    #user = None
    if user is not None:
        message = "Registro correcto"

    else:
        message = "Registro incorrecto"

    return HttpResponse(message)

def modificar_cliente(request, id):
    nombreEmpresa = request.POST.get('nombreEmpresa', '')
    rubro = request.POST.get('rubro', '')
    rut = request.POST.get('rut', '')
    region = request.POST.get('region', '')
    ciudad = request.POST.get('ciudad', '')
    comuna = request.POST.get('comuna', '')
    direccion = request.POST.get('direccion', '')
    telefono = request.POST.get('telefono', '')
    correo = request.POST.get('correo', '')
    contrasenia = request.POST.get('contrasenia', '')
    tipoContrato = request.POST.get('tipoContrato', '')
    fechaIngreso = request.POST.get('fechaIngreso', '')
    cliente = RegistroCliente.objects.get(pk=id)
    cliente.nombreEmpresa = nombreEmpresa
    cliente.rubro = rubro
    cliente.region = region
    cliente.ciudad = ciudad
    cliente.comuna = comuna
    cliente.direccion = direccion
    cliente.telefono = telefono
    cliente.correo = correo
    cliente.contrasenia = contrasenia
    cliente.tipoContrato = tipoContrato
    cliente.fechaIngreso = fechaIngreso
    clientes.save()
    return redirect('listarCliente')
    

